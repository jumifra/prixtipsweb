// console.log('hola mundo');

//document.querySelector('#resultados').addEventListener('click', traerDatos);
function load_store(list_store){
    // LOAD STORES IN CHECKBOX-----------------------
    // var stores = item['stores']
    var ul_store = document.getElementsByClassName('chk_stores')[0];
    ul_store.innerHTML = '';
    for(let store of list_store){
        ul_store.innerHTML += `
        <li class="resultFilters__int__drop__list">
            <label for="${store}" class="resultFilters__int__drop__list__label">
                <input type="checkbox" id="${store}" class="resultFilters__int__drop__list__label--button element_chk_store" value=${store}/>
                <p class="resultFilters__int__drop__list__label--text"><span class="resultFilters__int__drop__list__label--textSpan">${store}</span></p>
            </label>
        </li>
        
        `
    }
    // END LOAD STORES IN CHECKBOX ---------------
}
{/* <li class="resultFilters__int__drop__list">
            <div class="resultFilters__int__drop__list__label">
                <input type="checkbox" id="${store}" class="resultFilters__int__drop__list__label--button resultFilters__int__drop__list__label--buttonCheck element_chk_store" value=${store}>
                <label for="${store}" class="resultFilters__int__drop__list__label--text">${store}</label>
            </div>
        </li> */}
function load_brands(list_brands){
    var ul_brands = document.getElementsByClassName('chk_brands')[0];
    ul_brands.innerHTML = '';
    for(let brand of list_brands){
        ul_brands.innerHTML += `
        <li class="resultFilters__int__drop__list">
            <label for="${brand}" class="resultFilters__int__drop__list__label">
                <input type="checkbox" id="${brand}" class="resultFilters__int__drop__list__label--button element_chk_store" value=${brand}/>
                <p class="resultFilters__int__drop__list__label--text"><span class="resultFilters__int__drop__list__label--textSpan">${brand}</span></p>
            </label>
        </li>
        `
    }
}

function load_range_prices(min_p, max_p){
    var min = document.getElementById('min_price');
    var max = document.getElementById('max_price');
    var step = min.getAttribute('step');
    max_p += parseFloat(step)
    min.setAttribute("min", parseFloat(min_p));
    min.setAttribute("value", parseFloat(min_p));
    min.setAttribute("max", parseFloat(max_p));
    max.setAttribute("max", parseFloat(max_p));
    max.setAttribute("min", parseFloat(min_p));
    max.setAttribute("value", parseFloat(max_p));

}

function push_products(dic_product) {

}

function send_filter(){
    // INSTANCE VARIABLES
    var send_stores = [];
    var send_brands = [];
    var send_min_prices = 0.0;
    var send_max_prices = 0.0;

    // GET VALUES FOR VARIEBLES
    var span_min = document.getElementById('span_min').textContent;
    var span_max = document.getElementById('span_max').textContent;
    span_min = span_min.replace('S/ ', '');
    span_max = span_max.replace('S/ ', '');
    send_min_prices = parseFloat(span_min);
    send_max_prices = parseFloat(span_max);
    for(let chk_store of document.getElementsByClassName('element_chk_store')){
        if(chk_store.checked){
            send_stores.push(chk_store.value);
        }
    }

    for(let chk_brand of document.getElementsByClassName('element_chk_brands')){
        if(chk_brand.checked){
            send_brands.push(chk_brand.value);
        }
    }

    // SEND VARIABLES
    var data = JSON.stringify({
	  "min_price": send_min_prices,
      "max_price": send_max_prices,
      "stores": send_stores,
      "brands": send_brands
	});

    console.log(data);

    // var xhr = new XMLHttpRequest();
    // xhr.open("POST", "http://localhost:8000/lista");
	// xhr.setRequestHeader("content-type", "application/json");
	// xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
	// xhr.setRequestHeader('Access-Control-Allow-Methods', '*');
	// xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
	// xhr.setRequestHeader('Access-Control-Max-Age', 1728000);
	// xhr.send(data);
    //
    // xhr.onreadystatechange = function () {		//FUNCION QUE SE EJECUTARA DESPUES DE ENVIAR EL JSON
	// 	if (this.readyState == 4 && this.status == 200){	//comprobar las respues del servidor 200 OK
	// 		var product_json = JSON.parse(this.responseText);		//RECIVE EL JSON Y LO CONVIERTE
	// 		// CONSTRUIR DIVS DE PRODUCTOS
	// 	}
	// };





}

function traerDatos(){

    // console.log('dentro');

    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'final.json', true);

    // $.ajax({
    //     url: 'https://prix.tips/api/search',
    //     dataType: 'json',
    //     data: {
    //         term: 'bomberman'
    //     },
    //     succes: function (status){
    //         console.log(status);
    //     }

    // });
    // var data = JSON.stringify({
    //     'tern': 'bomberman'
    // });

    // xhttp.open('POST', 'https://prix.tips/api/search');
    // xhttp.setRequestHeader('X-API-KEY', '6aaXAZSSzRatgBVo7P12v7g13ovsu');
    // xhttp.setRequestHeader('USER-ID', '1');
    // // xhr.setRequestHeader("content-type", "application/json");
    // xhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    // xhttp.setRequestHeader('Access-Control-Allow-Methods', '*');
    // xhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    // xhttp.setRequestHeader('Access-Control-Max-Age', 1728000);

    xhttp.send();

    xhttp.onreadystatechange = function(){

        //console.log(this.responseText);



        if(this.readyState == 4 && this.status == 200){
            //console.log(this.responseText);

            let datos = JSON.parse(this.responseText);

           

            console.log(datos);

            let res = document.querySelector('#resultBody');
            res.innerHTML = '';
            var count = 1
            for(let item of datos){
                //console.log(item.company_name);
                if (count == 1){
                    // LOAD STORES IN CHECKBOX----------------------
                    var stores = item['stores']
                    load_store(stores)

                    // LOAD BRANDS IN CHECKBOX----------------------
                    var brands = item['brands']
                    load_brands(brands)

                    // LOAD RANGE PRICES
                    var min_p_x = item['total_min_price'];
                    var max_p_x = item['total_max_price'];
                    console.log(min_p_x);
                    console.log(max_p_x);
                    load_range_prices(min_p_x, max_p_x);

                    count += 1
                }
                else{
                    var name_product = Object.keys(item)[0];

                    // console.log(name_product);

                    if (true){
                        // console.log("si");
                        var min_price = item[name_product]["min-price"];
                        var max_price = item[name_product]["max-price"];
                        var image = item[name_product]["image"];
                        var list_store = Object.keys(item[name_product]["stores"]);
                        var list_logo = Object.values(item[name_product]["stores"]);
                        // console.log(min_price);
                        // console.log(max_price);
                        // console.log(image);
                        // console.log(list_logo);

                        if (item[name_product]["compare"]){
                            var prices = "S/ " + min_price + " - S/ " + max_price;

                        }else{
                            var prices = "S/ " + min_price;
                        }

                        var li = "";


                        for (let logo of list_logo){
                            li += '<li class="resultBody__box__brandsContent__brands__int"><img src="'+logo+'" class="resultBody__box__brandsContent__brands__int--img" alt=""></li>';

                        }
                        res.innerHTML += `

                        <article class="resultBody__box">
                            <div class="resultBody__box__grid">
                                <div class="resultBody__box__offer">
                                        <p class="resultBody__box__offer--number">-11%</p>
                                </div>
                                <div class="resultBody__box__img">
                                    <figure class="resultBody__box__img__center">
                                        <a href="#" class="resultBody__box__img__center__link">
                                            <img src="${image}" class="resultBody__box__img__center__link--src" alt="Imagen">
                                        </a>
                                    </figure>
                                    <div class="resultBody__box__img__icons">
                                        <div class="resultBody__box__img__icons__tool">
                                            <i class="fas fa-list-ol"></i>
                                        </div>
                                        <div class="resultBody__box__img__icons__tool">
                                            <i class="far fa-heart"></i>
                                        </div>
                                        <div class="resultBody__box__img__icons__tool">
                                            <i class="far fa-clone"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="resultBody__box__info">
                                    <div class="resultBody__box__info__int">
                                        <div class="resultBody__box__info__intScroll">
                                            <h2 class="resultBody__box__info__intScroll__title">${name_product}</h2>
                                        </div>
                                    </div>
                                    <p class="resultBody__box__info__price">${prices}</p>
                                </div>
                                <div class="resultBody__box__brandsContent">
                                    <div class="resultBody__box__brandsContent--prev"><i class="fas fa-chevron-left"></i></div>
                                    <div class="resultBody__box__brandsContent--next"><i class="fas fa-chevron-right"></i></div>
                                    <ul class="resultBody__box__brandsContent__brands owl-one owl-carousel owl-theme">
                                        ${li}
                                    </ul>
                                </div>
                                <div class="resultBody__box__compare">
                                    <a href="#" class="resultBody__box__compare--link">VER MÁS</a>
                                </div>
                            </div>
                        </article>
                        `
                }
            }

            }
        }

    }
};

traerDatos();
