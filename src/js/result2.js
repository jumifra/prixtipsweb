console.log('hola mundo');

//document.querySelector('#resultados').addEventListener('click', traerDatos);

function traerDatos(){

    console.log('dentro');

    const xhttp = new XMLHttpRequest();

    xhttp.open('GET', 'ejem.json', true);

    xhttp.send();

    xhttp.onreadystatechange = function(){

        //console.log(this.responseText);

        

        if(this.readyState == 4 && this.status == 200){
            //console.log(this.responseText);

            let datos = JSON.parse(this.responseText);

            //console.log(datos);

            let res = document.querySelector('#resultBody');
            res.innerHTML = '';

            for(let item of datos){
                //console.log(item.company_name);

                res.innerHTML += `
                <article class="resultBody__box">   
                    <div class="resultBody__box__img">
                        <div class="resultBody__box__img__offer"><p class="resultBody__box__img__offer--number">11%</p></div>
                        <figure class="resultBody__box__img__center">
                            <img src="${item.product_image}" class="resultBody__box__img__center--src" alt="Imagen">
                        </figure>
                        <div class="resultBody__box__img__icons">
                            <i class="fas fa-list-ol"></i>
                            <i class="far fa-heart"></i>
                            <i class="far fa-clone"></i>
                        </div>
                    </div>
                    <div class="resultBody__box__info">
                        <h2 class="resultBody__box__info__title">${item.product_name_unaccent}</h2>
                        <p class="resultBody__box__info__price">S/ 7.90 - S/ 8.90</p>
                    </div>
                    <ul class="resultBody__box__brands">
                        <li class="resultBody__box__brands__int"><img src="img/apgames.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/carsa.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/coolbox.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/efe.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/gallomasgallo.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/hiraoka.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/juntoz.png"  class="resultBody__box__brands__int--img"alt=""></li>
                        <li class="resultBody__box__brands__int"><img src="img/lacuracao.png"  class="resultBody__box__brands__int--img"alt=""></li>
                    </ul>
                    <div class="resultBody__box__compare">
                        <a href="#" class="resultBody__box__compare--link">COMPARAR AHORA</a>
                    </div>
                </article>
                `
            }
        }

    }
};

traerDatos();

